terraform {
  required_providers {
    nexus = {
      source = "datadrivers/nexus"
    }
  }
}
provider "nexus" {
  insecure = true
  password = var.nexus_admin_password
  url      = "https://${var.ingress_hostname}.${var.ingress_domain}"
  username = "admin"
}


provider "helm" {
  kubernetes {
    host                   = var.kubernetes_auth_host
    cluster_ca_certificate = var.kubernetes_auth_cluster_ca_certificate
    client_certificate     = var.kubernetes_auth_client_certificate
    client_key             = var.kubernetes_auth_client_key
    token                  = var.kubernetes_auth_token
  }
  /*
  registry {
    url      = "oci://${var.registry_server}"
    username = var.registry_username
    password = var.registry_password
  }
  */
}

provider "kubernetes" {
  host                   = var.kubernetes_auth_host
  cluster_ca_certificate = var.kubernetes_auth_cluster_ca_certificate
  client_certificate     = var.kubernetes_auth_client_certificate
  client_key             = var.kubernetes_auth_client_key
  token                  = var.kubernetes_auth_token
}
