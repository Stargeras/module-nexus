resource "helm_release" "nexus" {
  name             = "nexus"
  namespace        = "nexus"
  create_namespace = "true"
  repository       = "oci://${var.registry_server}"
  chart            = "${var.chart_path}"
  version          = "${var.chart_version}"

  values = [
    templatefile("${path.module}/values-tmpl.yaml", {
      ingress_hostname     = var.ingress_hostname
      ingress_domain       = var.ingress_domain
      storage_size         = var.storage_size
      ingress_class_name   = var.ingress_class_name
      nexus_admin_password = var.nexus_admin_password
    })
  ]
}
