variable "ingress_domain" {}
variable "storage_size" {}
variable "ingress_class_name" {}
variable "nexus_admin_password" {}
variable "registry_server" {}
variable "chart_version" {}
variable "chart_path" {}
variable "ingress_hostname" {}