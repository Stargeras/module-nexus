locals {
  nexus_chart_version = "1.0.2"
  nexus_chart_path    = "stargeras/helm-nexus/nexus"
}

module "nexus-install" {
  source               = "./modules/nexus-install"
  ingress_hostname     = var.ingress_hostname
  ingress_domain       = var.ingress_domain
  storage_size         = var.storage_size
  ingress_class_name   = var.ingress_class_name
  nexus_admin_password = var.nexus_admin_password
  registry_server      = var.registry_server
  chart_version        = local.nexus_chart_version
  chart_path           = local.nexus_chart_path  
}

/*
module "nexus-config" {
  depends_on = [
    module.nexus-install
  ]
  source                 = "./modules/nexus-config"
  tenants                = var.tenants
  nexus_admin_password   = var.nexus_admin_password
  nexus_anonymous_enable = var.nexus_anonymous_enable
  nexus_anonymous_user   = var.nexus_anonymous_user
}
*/