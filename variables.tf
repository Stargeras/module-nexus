variable "kubernetes_auth_host" {
  description = "The hostname (in form of URI) of the Kubernetes API"
  type        = string
}
variable "kubernetes_auth_cluster_ca_certificate" {
  description = "PEM-encoded root certificates bundle for TLS authentication"
  type        = string
}
variable "kubernetes_auth_client_certificate" {
  default     = ""
  description = "PEM-encoded client certificate for TLS authentication"
  type        = string
}
variable "kubernetes_auth_client_key" {
  default     = ""
  description = "PEM-encoded client certificate key for TLS authentication"
  type        = string
}
variable "kubernetes_auth_token" {
  default     = ""
  description = "Service account token"
  type        = string
}
variable "registry_server" {
  default     = "registry.gitlab.com"
  description = "Endpoint of registry containing helmchart"
  type        = string
}
variable "registry_username" {
  description = "Username for authentication to registry_server"
  type        = string
  default     = ""
}
variable "registry_password" {
  description = "Password for authentication to registry_server"
  type        = string
  default     = ""
}

variable "ingress_hostname" {
  default     = "nexus"
  description = "Hostname portion of FQDN for ingress"
  type        = string
}
variable "ingress_domain" {
  default     = "example.com"
  description = "Domain name for ingress endpoints"
  type        = string
}
variable "ingress_class_name" {
  default     = "nginx"
  description = "Class name for ingress resources"
  type        = string
}

variable "storage_size" {
  default = "8Gi"
}
variable "nexus_admin_password" {
  default = "password"
}
variable "nexus_anonymous_enable" {
  default = false
  type    = bool
}
variable "nexus_anonymous_user" {
  default = "anonymous"
}

variable "tenants" {
  type = map(object({
    docker_repo_name = string
    docker_repo_port = number
    tenant_sa_name   = string
  }))
  default = {
    tenant1 = {
      docker_repo_name = "tenant1-docker"
      docker_repo_port = 8090
      tenant_sa_name   = "tenant1-sa"
    }
    tenant2 = {
      docker_repo_name = "tenant2-docker"
      docker_repo_port = 8091
      tenant_sa_name   = "tenant2-sa"
    }
    tenant3 = {
      docker_repo_name = "tenant3-docker"
      docker_repo_port = 8092
      tenant_sa_name   = "tenant3-sa"
    }
  }
}


/*
variable "docker_repositories" {
  type = map
  default = {
    "tenant1":"8090",
    "tenant2":"8091",
    "tenant3":"8092"
  }
}

# Not yet implemented, default nginx certificate is used
variable "ingress_secret" {
    type = string
    default = "ingress-tls"
}
*/